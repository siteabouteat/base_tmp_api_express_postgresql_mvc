const db = require('../configs/db');

class PersonModel {
    async create(name, surname){
        try{
            const newPerson = await db.query(`INSERT INTO person (name, surname) values ($1, $2) RETURNING * `, [name, surname]);
            return newPerson.rows[0];
        }catch (error) {
            return error;
        }
    };

    async update(id, name, surname){
        try{
            const updatePerson = await db.query(`UPDATE person set name=$1, surname=$2 WHERE id=$3 RETURNING * `, [name, surname, id]);
            return updatePerson.rows[0];
        }catch (error) {
            return error;
        }
    };

    async getAll(){
        try{
            const personList = await db.query(`SELECT * FROM person`);
            return personList.rows;
        }catch (error) {
            return error;
        }
    };

    async getOneById(id){
        try{
            const person = await db.query(`SELECT * FROM person WHERE id=$1`, [id]);
            return person.rows[0];
        }catch (error) {
            return error;
        }
    };

    async delete(id){
        try{
            const deletedUser = await db.query(`DELETE FROM person WHERE id=$1 RETURNING * `, [id]);
            return deletedUser.rows[0];
        }catch (error) {
            return error;
        }
    };
}

module.exports = new PersonModel();