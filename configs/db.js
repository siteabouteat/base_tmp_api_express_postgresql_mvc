const Pool = require('pg').Pool;

const pool = new Pool({
    user: process.env.USER_DB || 'postgres',
    password: process.env.PASSWORD_DB || '1234',
    host: process.env.HOST_DB || 'localhost',
    port: process.env.PORT_DB || 5432,
    database: process.env.DATABASE_DB || 'chru',
});

module.exports = pool;