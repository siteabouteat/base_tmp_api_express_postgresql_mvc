const PersonModel = require('../models/Person');

class UserController {
    async create(req, res){
        try{
            const { name, surname } = req.body;
            const newPerson = await PersonModel.create(name, surname);
            return res.status(201).json(newPerson);
        }catch (error) {
            return res.status(500).json({error});
        }
    };

    async update(req, res){
        try{
            const { id, name, surname } = req.body;
            const updatePerson = await PersonModel.update(id, name, surname);
            return res.status(201).json(updatePerson);
        }catch (error) {
            return res.status(500).json({error});
        }
    };

    async getAll(req, res){
        try{
            const personList = await PersonModel.getAll();
            return res.status(200).json(personList);
        }catch (error) {
            return res.status(500).json({error});
        }
    };

    async getOneById(req, res){
        try{
            const { id } = req.params;
            const person = await PersonModel.getOneById(id);
            return res.status(200).json(person);
        }catch (error) {
            return res.status(500).json({error});
        }
    };

    async delete(req, res){
        try{
            const { id } = req.params;
            const deletedUser = await PersonModel.delete(id);
            return res.status(200).json(deletedUser);
        }catch (error) {
            return res.status(500).json({error});
        }
    };
}

module.exports = new UserController();